# Docker Registry

1. Add Helm Repository

    ```sh
    helm repo add qts.cloud https://gitlab.com/api/v4/projects/35412855/packages/helm/stable
    helm repo update
    ```

1. Configure overrides

    ```sh
    cat <<END > /tmp/override.values.yml
    volume:
      size: 10Gi
      storageClassName: local-path

    ingress:
      enabled: true
      annotations:
        cert-manager.io/cluster-issuer: qts-issuer
        # traefik.ingress.kubernetes.io/router.entrypoints: web
        kubernetes.io/ingress.class: "traefik"
        #? kubernetes.io/tls-acme: "true"
      hosts:
        - host: registry.qts.cloud
          paths:
            - path: /
              pathType: Prefix
      tls:
      - secretName: registry-ingress-tls
        hosts:
          - registry.qts.cloud
    cleanup:
      config:
        repositories:
          ibacalu/alpine: 1
          ibacalu/ubuntu: 1
    END
    ```

1. Deploy

    ```sh
    NAMESPACE="registry"
    helm upgrade --install --namespace registry --create-namespace -f /tmp/override.values.yml registry qts.cloud/registry
    ```

1. Enjoy!
