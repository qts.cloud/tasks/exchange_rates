# Task Description

As the popular trend of Cryptocurrency arises and more and more businesses look to
accept Cryptocurrency, X eCommerce are looking to take the next step and take
crypto coins as part of payment.
As the Infrastructure Engineer leading this project the eCommerce team has asked you
to build an application to be displayed on the team's main screen showing the current
exchange rates.
Task:

- Build: -> [SOLVED HERE](build)
  - Write an application to consume api: https://api.coingecko.com/api/v3/exchange_rates
  - Display results in a presentable manner (can be written in any language)
  - Containerize application
- Helm: -> [SOLVED HERE](helm)
  - Write a helm chart to deploy the application on kubernetes / minikube
- CI: -> [SOLVED HERE](.gitlab-ci.yml)
  - Create a pipeline to build, test and push your container and helm chart to repositories in an automated fashion
  (can use any CI CD tool)
- Demo:
  - Prepare a presentation to present and demo your challenge to the Infrastructure
team
  - considerations:
    - security
    - monitoring
    - resiliency
    - production ready
- Advance
  - Write IaC using terraform to host application on EKS AWS
  - Deploy application to EKS
