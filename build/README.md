# Exchange Rates

## Task

Write an application to consume api: `https://api.coingecko.com/api/v3/exchange_rates`

- Display results in a presentable manner (can be written in any language)
- Containerize application

## How to?

### Prequisites

1. Docker Engine [Install](https://docs.docker.com/desktop/mac/install/)
1. docker-compose (comes pre-installed with Docker Engine for Mac)

### Check & Update files

1. [Dockerfile](Dockerfile)
1. [docker-compose.yml](docker-compose.yml)

### Build the image

```sh
#? Build image with defined args in the docker-compose.yml.
#? NOTE: Image is already pre-built and available on dockerhub
#?      docker pull ibacalu/exchange_rates:0.0.1
docker-compose build
```

### Push the image to registry

```sh
docker-compose push
```

### Docker Scan (trivy)

```sh
#? Install prequisites
brew install trivy

#? Scan Dockerfile
trivy conf --severity HIGH,CRITICAL Dockerfile

#? Scan Built Image
trivy image --severity HIGH,CRITICAL ibacalu/exchange_rates:0.0.1
```

### Run image & Check Logs

```sh
docker-compose up
```
