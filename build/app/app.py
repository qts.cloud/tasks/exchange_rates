#!/usr/bin/env python

import sys
import logging
import yaml
import requests
from decouple import config
from flask import Flask, render_template

CONFIG = {
    "config": config('CONFIG_PATH', default='/app/mount/config.yaml', cast=str),
    "url": config('API_URL', default='https://api.coingecko.com/api/v3/exchange_rates'),
    "log_level": config('LOG_LEVEL', default='INFO', cast=str)
}

logging.basicConfig(
        format='%(asctime)s %(levelname)s %(message)s',
        level=CONFIG['log_level'],
        stream=sys.stdout)


def curl(url=CONFIG["url"], method='GET', timeout=10):
    try:
        response = requests.request(method,
                                    url,
                                    timeout=timeout)
    except Exception as e:
        logging.warning("[{}] Warn: {}".format(url, e))
        response = {}
    return response


def load_config(path=CONFIG['config']):
    '''
    # config.yaml
    rates:
      Name: name
      Token: unit
      Value: value
      Type: type
    '''
    try:
        with open(path, 'r') as file:
            config = yaml.safe_load(file)
    except Exception as e:
        logging.warning(
            "[load_config] Warn: {}. Could not read file {}. Using defaults.".format(e, path))
        config = {
            "rates": {
                "Name": "name",
                "Token": "unit",
                "Value": "value",
                "Type": "type"
            }
        }
    logging.info("Config Loaded: {}".format(config))
    return config


app = Flask(__name__, template_folder='templates')

@app.route('/', methods=['GET'])
def index():
    data = curl().json()
    structure = load_config()
    return render_template('block.html', config=structure, data=data, title="Prices")
